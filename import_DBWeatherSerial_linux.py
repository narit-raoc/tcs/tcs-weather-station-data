import pandas as pd
import serial

from datetime import datetime
from sqlalchemy import create_engine, false, true


"============= Connect to Serial of Weather Station =========================="
def import_rs232():
    port_name               = '/dev/ttyUSB0'
    baudrate                = 9600
    timeout                 = 6
    serial_weather          = serial.Serial(port_name, baudrate=baudrate, timeout=timeout)
    weather_data            = serial_weather.readline()
    str_weather             = weather_data.decode('utf-8').strip()
    serial_weather.close()
    return str_weather

"==============  write data to dataBase  ===================="
def setDB(user,pw,host,dbName,tableName,time,local_time,WindSpeed,Rian,Temperature,Humidity,Barometer,WindDirect,WindChill,Dewpiont):
    dataFrame = pd.DataFrame({'time' : [time], 'local_time' : [local_time], 'WindSpeed' : [WindSpeed], 'Rain' : [Rian], 'Temperature' : [Temperature], 'Humidity' : [Humidity], 'Barometer' : [Barometer], 'WindDirect' : [WindDirect], 'WindChill' : [WindChill], 'Dewpiont' : [Dewpiont]})
    connectionString = f'mysql+pymysql://{user}:{pw}@{host}/{dbName}'
    db = create_engine(connectionString)
    dataFrame.to_sql(tableName, con=db, if_exists='append', index=false)
    del dataFrame

user = 'weather_nuc'
pw='asdf1234'
host='192.168.93.182'
database='weather_station'
tableName = 'weather_data'

while true:
    try:
        data = import_rs232()
        x = data.split(',')
        time            = datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S")
        local_time      = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        WindSpeed       = float(x[2])
        Rain            = float(x[3])
        Temperature     = float(x[4])
        Humidity        = float(x[5])
        Barometer       = float(x[6])
        WindDirect      = float(x[7])
        WindChill       = float(x[8])
        Dewpiont        = float(x[9])

        setDB(user=user,pw=pw,host=host,dbName=database,tableName=tableName,time=time,local_time=local_time,WindSpeed=WindSpeed,Rian=Rain,Temperature=Temperature,Humidity=Humidity,Barometer=Barometer,WindDirect=WindDirect,WindChill=WindChill,Dewpiont=Dewpiont)
        print('{},{},{},{},{},{},{},{},{}'.format(local_time,WindSpeed,Rain,Temperature,Humidity,Barometer,WindDirect, WindChill,Dewpiont))
    except Exception:
        times = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        print('{}   [DEBUG] COMPORT Disconnect'.format(times))
        pass
    
